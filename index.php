<?php
if (isset($_GET["show"])) {
    $arr = array();
    $_GET["show"] = urldecode($_GET["show"]);
    foreach (new DirectoryIterator('./'.$_GET["show"]) as $fileInfo) {
        if(!$fileInfo->isDir() || $fileInfo->isDot()) continue;
        array_push($arr, $fileInfo->getFileName());
    }
    sort($arr);

    $wf = './'.$_GET["show"]."/watched";
    $file = "";
    if(file_exists($wf)) {
        $file = file_get_contents($wf);
    }

    foreach ($arr as $key) {
        $arr[$key] = [];
        foreach (new DirectoryIterator('./'.$_GET["show"]."/".$key) as $fileInfo) {
            if($fileInfo->isDot() || substr($fileInfo->getFileName(),0,1) == "." || $fileInfo->getFileName() == "metadata" || $fileInfo->getFileName() == "backdrop.jpg" || $fileInfo->getFileName() == "folder.jpg" || $fileInfo->getFileName() == "logo.jpg") continue;
            if( strpos($file,$key."uwu".$fileInfo->getFileName()) !== false) {
                array_push($arr[$key], $fileInfo->getFileName().".!!!WATCHED!!!.");
            } else {
                array_push($arr[$key], $fileInfo->getFileName());
            }
        }
        sort($arr[$key]);
    }
    echo json_encode($arr);
    die();
}

if (isset($_GET["watched"]) && isset($_GET["folder"]) && isset($_GET["add"])) {
    // requires folder and file
    $watch = urldecode($_GET["watched"]);
    $folder = urldecode($_GET["folder"]);
    $add = urldecode($_GET["add"]);

    $wf = './'.$folder."/watched";
    $file = "";
    if (file_exists($wf)) {
        $file = file_get_contents($wf);
    }

    if ($add == "true") {
        if (strpos($file,$watch) !== false) {
            //ok
        } else {
            $file .= "\n".$watch;
        }
        file_put_contents($wf, $file);
    }
    if ($add == "false") {
        if (strpos($file,$watch) !== false) {
            $file = str_replace($watch, '', $file);
        }
        $file = str_replace(array("\n", "\r"), '', $file);
        file_put_contents($wf, $file);
    }
    die();
}

if (isset($_GET["pinned"]) && isset($_GET["add"])) {
    // requires folder and file
    $pinned = urldecode($_GET["pinned"]);
    $add = urldecode($_GET["add"]);

    $wf = './pinned';
    $file = "";
    if (file_exists($wf)) {
        $file = file_get_contents($wf);
    }

    if ($add == "true") {
        if (strpos($file,$pinned) !== false) {
            //ok
        } else {
            $file .= "\n".$pinned;
        }
        file_put_contents($wf, $file);
    }
    if ($add == "false") {
        if (strpos($file,$pinned) !== false) {
            $file = str_replace($pinned, '', $file);
        }
        $file = str_replace(array("\n", "\r"), '', $file);
        file_put_contents($wf, $file);
    }
    die();
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>top 5 anime ui of all time</title>
    <style>
        * {
            box-sizing: border-box;
            user-select: none;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        p {
            margin: 0;
        }

        body {
            margin: 0;
            padding: 0;
            overflow: hidden;
            font-family: Arial;
        }

        body > div {
            transition: opacity 0.3s;
            height: 100vh;
            width: 100vw;
            overflow-y: auto;
            position: fixed;
            top: 0;
            left: 0;
            opacity: 0;
            pointer-events: none;
        }

        .c {
            display: flex;
            flex-flow: row;
            justify-content: center;
            align-items: center;
        }

        #animeList main {
            display: flex;
            flex-flow: row wrap;
            justify-content: space-evenly;
            max-width: 80vw;
            margin: auto;
        }

        #animeList input {
            background: transparent;
            margin-top: 16px;
            margin-bottom: 32px;
            height: 32px;
            border: 0px;
            border-bottom: 1px solid grey;
            transition: border-bottom 0.3s;
            max-width: 300px;
            margin-left: 32px;
            margin-right: 32px;
            width: 100%;
            border-radius: 0px;
        }

        #animeList input:focus {
            outline: none;
            border-bottom: 1px solid pink;
        }

        .showCard {
            height: calc(1000px / 3);
            width: calc(680px / 3);
            border-radius: 24px;
            margin: 16px;
            margin-bottom: 32px;
            background-size: cover;
            background-position: center;
            box-shadow: 4px 4px 16px rgba(128,128,128,0.5);
            transition: transform 0.3s, box-shadow 0.3s, backdrop-filter 0.3s, border 0.3s;
            cursor: pointer;
            border: 0px inset lightgreen;
            order: 1;
        }

        .showCard.pinned {
            border: 6px inset lightgreen;
            order: 0;
        }

        .showCard:hover {
            transform: scale(1.05) rotate(2deg);
            box-shadow: 0px 0px 16px #C0808180, 0px 0px 64px #C0808180;
        }

        .showCard p {
            border-radius: 24px;
            opacity: 0;
            transition: opacity 0.3s;
            backdrop-filter: blur(20px) brightness(0.5);
            position: absolute;
            bottom: 5%;
            left: 5%;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            font-size: 1.25rem;
            padding: 4px;
            min-height: 20%;
            box-shadow: 0px 0px 8px rgba(64,64,64,0.5);
            width: 90%;
            color: white;
            pointer-events: none;
        }

        .showCard:hover p {
            opacity: 1;
        }

        #folderList {
            backdrop-filter: blur(20px);
        }

        #folderList main {
            opacity: 0;
            transition: transform 0.3s, opacity 0.3s;
            transform: scale(0.8);
            max-width: 60rem;
            width: 100%;
            height: 80vh;
            margin: auto;
            margin-top: 10vh;
            border-radius: 24px;
            box-shadow: 4px 4px 16px rgba(128,128,128,0.5);
            padding: 16px;
            background-color: white;
            overflow-y: auto;
        }

        #folderList details * {
            margin-bottom: 16px;
            font-family: monospace, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol" !important;
        }

        #folderList summary {
            cursor: pointer;
            margin-bottom: 8px;
        }

        #folderList p {
            margin-left: 14px;
            word-break: break-all;
            margin-bottom: 8px;
            cursor: pointer;
            transition: color 0.3s;
        }

        #folderList main > p {
            text-align: center;
            margin: none;
            margin-bottom: 16px;
            cursor: unset;
            font-size: 2rem;
        }

        @media (prefers-color-scheme: dark) {
            * {
                color: white;
            }

            body {
                background-color: #222;
            }

            #folderList main {
                background-color: #222;
                box-shadow: 4px 4px 16px rgba(32,32,32,1);
            }
        }
    </style>
</head>
<body>
    <div id="animeList" style="opacity: 1; pointer-events: unset;">
        <div class="c"><input type="text" placeholder="search"></div>
        <main>
            <?php
                $arr = array();
                foreach (new DirectoryIterator('.') as $fileInfo) {
                    if(!$fileInfo->isDir() || $fileInfo->isDot()) continue;
                    array_push($arr, $fileInfo->getFileName());
                    $dir = $fileInfo->getFileName();
                }        
                sort($arr);
                $wf = './pinned';
                $file = "";
                if(file_exists($wf)) {
                    $file = file_get_contents($wf);
                }
                foreach ($arr as $dir) {
                    $bdir = "./".$dir."/";
                    if (strpos($file,$dir) !== false) {
                        echo "<div class='showCard pinned' oncontextmenu='pin(this, \"".rawurlencode($dir)."\")' onclick='show(\"".rawurlencode($dir)."\")' style='background-image: url(\"./".rawurlencode($dir)."/folder.jpg"."\")'>";
                    } else {
                        echo "<div class='showCard' oncontextmenu='pin(this, \"".rawurlencode($dir)."\")' onclick='show(\"".rawurlencode($dir)."\")' style='background-image: url(\"./".rawurlencode($dir)."/folder.jpg"."\")'>";
                    }
                    echo "<p>". $dir . "</p>";
                    echo "</div>";
                }
            ?>
        </main>
        <div class="c">
            <img style="image-rendering: pixelated;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPsAAAGWCAMAAACJsQWkAAABjFBMVEUAAAD///9VVVWSbW1NTU3/v9VJN0lDNkOzc4z/schANUBANzc+Ljb/tMvDgI+AV148LzY6LjT/tczZiJ1kRlA3LS2tb33/vc/YiZ1VPkaWYnG2dYf/w9XThpt8VF3/wtZVPEHaip94T1u5d4j/w9VbPUXdi6P/wdWAVWBkQkv/wdPaiqCNXGj/wNRwR1H//////v///v7+/v7//f7//f39/f3+/P39/P39/Pz8/Pz++vz8+/z8+/v++fv9+fr7+fr99/r89/n99fj89fj98/f78/b88fb88PX77/T77fP57vP76/L46/D76fD65+/45+765e725e354+354uz54Ov34erx4+r43en42+f32+fw3ub32OX21OPu2OL10ODr0t70zN3nzdnyxtrwv9XhxdPutc/vnsTunsTtnsTrnsTtncPqnsTtnMLrnMLmnsPsm8HnnL/qmsHpmsDfnsPpmb/kmr7nmL7ml73cnLzllrzjlbvhlLrfkrjYlbfckLbZjbPVibDPhazHfqWDQnQT6s6uAAAAL3RSTlMAAgMHCgwOExQXGBwhIiImJiwtLzMzNTpBQkRGSFJSWFpgam1udneEh5aXmaaqtqkRCN4AACYFSURBVHja7JrRTuM6EIbbJkqkJBKJRC56s0JHQlokBJQuaVynLV042z37/g90Zsa1ndSoSgPSyo4/geACaL78M2PHdOLxeDwej8fj8Xg8Ho/H4/F4PB6PxzMWOJ+MFN40zUjlOQD2kxHCjzTTycgIYsYYxg724WRUhClTufM0mowIVGcE2Wcjkg9TJc7YuOQDUidvhI1IfpYUDKgFjOR5Oo6BF+dkviLAngqgSGYT94lQHc1fALJnJB9PnCdISwr9hSB9IZ+73/LY7EK9QpQ8L7Ng4jZxTupo/oxUlZIvkonThFl5VH+WSHnnqz4ptHpXnjPmdtVHOc45UjfkHa96nPGmOsk7X/Wi4kndlKeqT12t+iiXzW5QUdW7u8OZpWWn2ReLp6fF4qTqMzf39VHRaXYwf3wE+07V8zJx8QgryMqu+iOh5OW4c7Hqk0I2u1LX8jp4F8ddmBnqUr4VvJvjLulUPKhr2sFzXjo37qK80+zkbFS9o880aWlUvFH1x+Bd291FuaH+YfAr5tw6N03Lk2YfT/BR0Y3dQAcv1jl3Di5nMnZT3VznOI47d4KPizMVb65zjUMbnLOxm+scd2mDE5mxmzy1g3fm0LYb+6OJGTx3Jfio6FZ8r+C5E8FPT2IfU/C4pWvH3jP4xoXgT2IfU/Ah7eR17D2CX2HwLmzuEiP2nsHbv7kLs1q7Q+wXBW/541xS1EbJ9wq+sf5xLqBTOh173+BrCt7uA5yIYq9U7BcF3+RWn9ylZW93wqFlLsxZ3bPkdfDkbv/+Ji7YamXE3jP4xuplbpayk5Ifz7SL8rY7qo1n2iUlubfafTTTDkqe1Ua7XzbtbN3Ui5If4A5Ulh/Vx6XhflHR15zZOu2mKTNW98unXWZl0Yc5667uIyr6uMDYB7gvpLu97zdMhrS7+SRr41P8LGO1XuEGugM2bm/oOUa3+yB3W99uKNp9gLtseJC3dZVLdMlTu/d3Pyl6C1e5tF3yQ90heAsbPsgYxm64X7TKrWor34gQ5vWJ+3gaPipaJT/EXTd8OrGMuNPuF835tnvNLfx3dFKDezXQ3e5hN02/xt3GYRdkJ+695Z+0e0U1b93jTJi33VEe6Kvedbdu0Eddd7JH+qgDR/VVbeP5RVwI90q6kz1xzhzVERV73QB2nV9Mk7IG+UrJa33iY3Ey1+oVxQ5cW/UoF2bKneSXS0Of0OLSHFmCOXKMfXed/PWinwK9233OSF6A8qSv/U0W0nxJ5qS+bXbA/CrqeYXI10oHUZwkSSqA7+I4CoNzUQRpMW9qoNKAvvLXSG8lrszXYL7dEfOzwc+CMIqPF5hlGV1iHAVfIT5jlCBrURZFDi8CrxB+fE1xPr9pSH69Xlearr7JEhDi63W93oL7HtT3366voo/iROkEfPOiKJmEI/Tlc/qccQ7qDKkl+kXwFqSJcQOmUXaN7tstGkjO6GvxH8Ic2AKve8G3OcjPuomEaJ2TswkXNMDn1AlIXtL15yXcAKwxFUwYozq5E5vNpuu/PNIRR5T4ZgPir6j+9gYf/6B8LG/xDNPuardiwdABNIeP3WB5znnL/EWg/NV9IX9qASROc1S/hSm1lWyITvwGP7T4BsRR/Q349w3cSf4qiYEEmlqVeK0Rl0WQPxfmu6HuTKlLc9OfK3+cAkBxXaL67W6/f22j/SuN9m6ZvwI/f5I48f07yIM9An+95AyRyiZoT6kDu93gmif7o3mlaPkjqszmghtUv7vbAyChACOtb0LiAP4Kmr+//wLvA3zekzwwB8RrKW0ZSjcYKb/7hLtMncyfFaY/k8P1BrlF9TtqVwA13jv+a4FhLsXfwfvX4XD4LbhHeeDmBooYnVBcWVcSHQvJ44/ugMHuaE/q0vyMv5ysaA48CHPUEICR1Jf+Gh05eaP2f8SfP7/vQR7A5Q7lOZlr7dPrEvJ4LZ9xD5mseEOdUHda64smA/OHBzTX+R0O/zN3vz9NZGscwFdcdoW4uhd31V3XbExITMzdFzdriJdE+U2BWuy2JYVACGnDKahYIMqNSvx1/vH7fZ4z5zwzPOO00g7wfaG7aNv59HnOOTPTAxbYT3pEpj+Gi5zhcBddnj5dWvo3ZW5miumQ8xF5tQ7je7b/wHZFT28Az+eXnIKd5eRgCH6lN4D1zJc3IFZywNldKpWWOaXS06K1S0sY9s7+iIeghusfm+LoP57W/gh0sWf6Rf+E7FNT0zOzBnKGuBSLxEfxZ/zYN5MuQc5/n9jlcrlSqeDXZ8slg76fm8NKPzXB9Gw5ErcPn7bnYZe7reE2xNf5Ef4J7Kg6KAYSY0wZAcjxC8yHX+LlKDngYFerNUoVjwUefV8AHvYnckckI2N9sF++GtlFLpceqc0PPNmfkH0OFsiXy8bAUK2iisxfjPjwm5kAh9yVHHCoDVKv1w3jYV9g+xPYFf0/IWLvvecHTtghV7eWlJ7tk1NUdtAhdwGjBj/4XHzmz5tZhOFOjpIDXjerq6smpFIuPV2M7ONCF3bsKjhun2D7qW/5DItd6MJXfnfVPe7s4ABTCQZwarWgB3/BmILLQlJuEqmWl0toFdgnn/BVsXKHIxI75ly2n/7e/o+wqw+SNV/87jQFp+MzM1x2okvWzWp9pVYtl6n14TcG74BbCEpOXl/FXzqRlWq5VFwozLJ9PKIrOIfwYqdcvXz6D9WAV/YsfsxOZa9UvUD0XPxn4JeMKXIy5EgdhS8uwj4pdn3XW+w8z/83smO3Tg+TndovlKEX+zRaHmUPdMGvr9XrK27mM6aE8JJWIfmqSU2tQvY5soM+5ukkz7BPcIZ72EAgA17smXq2Y7meQ8svl2tasrEOPYoPvjFlCuCQS9GVvYoBX5gXu9C1PdnyP//Q26aZR53soo/Z5wto+SrsGm+hp+KvGFPl1FYgF3hK02Oym5+ZnnxMdqF3tl8d6G1vZLDrLfAaz/bHaPl5bnkUU2fTbmwQf40mshW462tr60LX9homu+7sJ4f7cO97YlXhVSL7396OqU5aXuM3N8A3yBoC+PqGybQvwz5NdtD/dvav70OWFY5aPt+ml8KLfZaGe8XZdbbs5uYmc9fJjZiMrNYqZJ/Vdt3yeoXLu+mRyP632DHc6+ar+K2tTW4BuPE+mG+yZ7f8WGj5fw33YVesKnym/XFkL4tdpWG2tizZKVvmm+yZK5xu+TOa6cU+T/aa2DW+0SD78y3ENL7B/lDsnVpevuGkp9ObMcF3aS/CLguXTrNpDdKgdKp7Vdm7Oamjnbj5Nz3oCfv0NE/z2q7TaDaN6aNdrmN+u/p97zso9PVMOv0b7ag7Kg968zR2hVfXMb8O9+PH06glXtG7tetsQ35Ke/KSOrR8dOn+m8x0vcx2alewop/ObsluTm1HFD1Wdsx0ff0GCOD1OFP2yY52wW8Lvvt5HlH2+EwnC1xOhdd02B86+yzZK7XVDdNbsu2I0BMzHez9+VFo36vCK/qp7S+76Xl/Pv9Y2RHZjih2tcD1tszJEs94oedb92y7RMrOLS+n8r0vc9L0gk+jwz6ev13jddmH+/iDdROFRwQudNjH+m9fh70Ut2s8/i98IOPKTuc1ORUeUXBlp3m+b/ZC0s6J00PZYe9n2XELI1F4RLlztNdhXwz2MdhVxM70X6nseRVeh+n528dhV/hk2YFH2fuXASk8JYOetJu+2KsJ+0OH1/Qcyi6Fz8L7PUNY4nK26y168knUIyl7fwuv8J6MCL3v9tV6ddnZJ9lOlQ8JdN5uDzrCZe9z4WVHPCfJfgg405290Df7RtzOeOYHv5eD7sv+Yx4/tMThI/+YSqD3114TO/DQ6wQ6wmfy/S+84NPhRGf7PN+ixjy/0ftwd3ZcywR8it9/kwXS37LLPQxK0I+rQO7KDnupr/Yi21O3p/m99kyfQNkH+m6/9MPP2NrmEsOqTLJ9AfYq7KYvdv4APuAl4z68zcfRc/nmmoHhXzCeEvuDtRtyorsPoGtrfbFHH8DPG785L/kO+AOCHPSJYZQ9j8LjnfX6yZDpZBydPoCu1td6b/lNttOAN/OovGQyFhzPhMtvtMkkn8JPINg7J2QNn3V0Gu59sWONq8FeNG53FjJ9IrzTHsGh4duKcqGj8Nfx7FPIjMssQVUKBbNYpLKv1Nc3Nrd6tvOAX+a9OQXwJbOUmRDS/3Id61tehWf4HNSs5CxwCj68fwplh30NdtO7nQpvjNucJSm4zCNzs9iBxrmO9S2nXLkOOMWJFylFl0WK/4IxREfL92zfcnZj3L7EKPKK0bswx5n55fpAjj+R7iZeYmFhaYmJJRcclcR9wRiic8s3Uk0tSld2whvDe5M4pUSKCG80X8CB/U7fR5dj4X8HHG4AkXIyy4j7atUYosPeaIhVp7OdCm94VyL4KssIvwNLxL955VKO9svXbi4tFeF+hm3OtEsoliqlgi8jK7ylAvStxlfhtgt8o4HC07Uc+l7HvWCZN+tB//v1KznSUfibdzCN/VMBr1ZHVkPqCI5nZYX/YM2YHaY3TLPV2nFRdqSTHYXnnSmrFP2K9doKdcSz8vLTO7mWHRm8duduhTeF0R4hw/G/4fgoa4jbN0X0Zmt7Z2eXInyxa3zyi03geWPKegi/kotBeLca/P88u3MNZc8zA9du3sW2MOdOZmNDDmvDzfA7jWazyfCgz7afmAnw6IaBHdmg4EX0VkXyg1+9eQ1lzzVXbty9l7EbDmJyI1tMaTa37QtK0GfY1Ty4zRsTtpAO+5HW1+7dvnElZ/qlwRu3761bI0nRA440TMvaF9vbTGf9c47YXRQ9TA7b227M06g3HXL39rXLOdsx4G/fMx3T2MIBN+0u4VH4V14veInQRe7027G+72i/MfRd3hkYun23C7tpIruw21c+wO9BrvSavsuh0vu275zbNwa/yz1DN0xX2Wmi3S3j2b/36sUeRelFjjxHYlOjpbbvzv7TpfzrPmi6SmtndxvtTmH7/qv9vf1U/Am66DFkdlqmy6Dl80/39l0b8Pug7+8fsv2lx4s9VY58A35k8Azsl7umv3iBelunf0P0Q4dX9rh8j4O5gejo+1aX+J/Own7JtLqgsx14G+HfOPzBgS58Gl3sXeHx8CEM9/wzhFfqXPbI/irYoX+dao/TX+5FIfqL3e7wvDrQcM8/g3xq1rnsrucFfwT74UGb7RxFD3ZMiqB3ZY/WRWn5fO0/CT7bzvj9N95+9Po17LrwuuxM93aN1/RdseebIRwOpaOd6fvodot4uyq8KjvT0fZib2XK6bWk5XM+pR/ReD0Aue5Y2jz+SNuFnrC3Dw4s9X0ne0vossLl3/QRvpVl58LvMZ3xyo6InOlsb7cP7MEeZ/e52LPoz6Xl8296jdd2woOOHFGsJbrYJcpuvR1fcy+UQedHSsvnX3jBpx8W45/Dvrd/GPBiF7y2t2FvtzvZWxzAOXuq7LkW3uHT9b4iuzh8nMy9fo12RyzTs+0I6MGOiF3RnRtR1zG5Fj4cFScVHx3XAek5hzbTDjrbLcHF3qKkyAMdkZbPP5eGRvTFWLodeOg5mL6JDjuSSnd4TW8pOeLl7T010+Ve+KDXfDk2xvtADjvhfZLwpJ3/1D+3lgu9jbKfZVB40St+K4mHmOEcD0uPyEFX9pZP/Eyorcqee+HxqoovtyIUXuiZseyWllBPquTtsyy74BMrdSstcown5O1khJ4cDGnPKHJ+npEzLLtMd745M/yydCHKfcBJ+oVO6QRHzpQuhZd8tVKyevnE2H76R0QvdCTtqZLPM6JuWpwNXppVOkAfMCfMY16ONV9yeMh8XtnjdC0P7gOXW2dcdpnr2yFJfgZe5EcS4svqrweRwMXNOTxzugz55KhVt2HFznh/5Jbh/0Pevn1Lv3m9PutLXt/6nuEzxUPk7Dteun4kjNvIH/RIZBe8o6Po9ojZ71zgh55P91XhRY6cHCxH59Hxgr9F7sSgFb1E7Dh6Krol+Hsf5lvUXi7zUuUMl8FyTnS5hXPLVQBhv9ZrOopuCX7sQ3qL2iu8apgwVnikjAzlLswe8rfIzYcio1bwCnBI9LfvLMk/fLAfENLbd281Xha1ICc3B3+dBvt5ZnBohI6GD4j50GcUz9PfH5P640f7EcF/WfsefQ/8oZ/vXibkbS/HWPE5x46XIe+PJ+J7PAAi9x0PANOPLbPtJ6e33PiCZz0SH+gkl1nC3jrPjpchb+NzFvCx6gW5DHamf/hoLeCUT58+WtQe+FjlJW2m83uGR/ppwtpz7XgZ8n/YYxwR692wTVYvlf7xE9PZjh4gPBee8F4fVnTud9Sc4Sy359zx0vV/WHuMEF/wqdUTOss/f/7s+Aofgod5Ost5drB/oOMvQqjwCOsVXjUuasd0oEM+M/5Y4TWd5aBfjLJz4f9kgsfzmJfTXDEQAfQPQv/yReH5jUP4UYH+NqJbyp8Xpew84hnP+mixSutc6Ximsx0RvJ/s+dGIo0fLIi0OCNsvStkx1cM+aqNI4T1e6KHsYveFB94XPuCZLh1vfUYvTMu7ph+9b0OiEe/x0rlSdtglbE8OedIjTHcngpJRavmL8q8FO/uDuEXsRFdlV/YvjHdzPeMpoDu7jef+6MUZ7sH+V6Lu1pKZ6VJ2nuS1nRMb8YT3sQgeGQr/4OLa3/vCcd0tJ2a3neMeb6PggfTIMNf99eD+her5oZHIzksc1y25TtlTxVcedo8/dvaLM9cNYo1j+/tAz5jq0p082/uZPj7bncC//+sB4W9dlKank9rR0fvR+bzQ21FSx/sXhefJTuxMPyC9XL3yS1ykwuOu3f+Zu9+fKJI0DuBh2QUPf4wEJCwcJoYLb3xjyBnxcBkmA5GQUWRxVBwWb9elGUJEI8FwyxG91D9+3+epevrp6mdoIJke+e5qxETgw/NUT3dXTxXK/he+NZUrXfGwy3Ge7Hn8/1B2+wqPZPXg/3Xyn3+g8FdixPfRaNd7rl+03yO8Nj3bFR9OcfA3apfPoXo+4vkbnPh1hMJ/f3wf0f9OD5WwO/Sqnssjel6nTU92F9vl9T0+pUdE7/lyS/sK4In+mb417xa5wCP8F72UIXzU8KbsZtBAz3zfWAjwPR7zls4PF3yW4M/RtKLo4xsXwBec02bp9HCB8BH4Q098+PA98ToZCz4nhudmorTwhM++tsW3L9jOdHPzQwI4p5fzz3as88MFNnBLzB0rxju1h+TKrnQbfR7nu+GJzsm5Ba562/XuGzc6/S83rjzd23V+S2LhPje/y0lOv9AlRm3nJWBnvPtKY/4b7BT8iRohd9PKy6PPE6kF38vDvVa9zTFUMwNtut6h8ox333zwEfBE15aPW8imHbLXa7zSEbWamMIT3lHbQw+wd/NhDnju+EzL52fzRR1lt+eV/9tNZRcmLjzhHV+VyJwUIpNyescnthekTentmO/T58jPtyNsD3gIcVquc5FIOhnrUPbcbK6lW35Pj/ZEL1y8IW/XyVTnZHYJYEdqJJ2E15td0cPExeklHoPdqE2sXWca0gtSx24kncl00vLWntik+vKHvN6oKXLH7+0Ve4wnPeKIjeg1oL/PFz1AH/CFX6fduzeNFLnj7wl2fbTUeXzQIw5qSnQR6KKWF/s5Xyrp61HZkwvE0xG1g0540SOOflM4yRHg07KnC0Wc98WS3pQ9ubw9fz2LkNeBzW6FU/S9QrpORFIcFP7KlN3j22fgVW/hHxHn6YjYk3Pz05UpO+yBvid2q5d4d+4hzX3YORe0X6E3wIt9bw94B7wpvT6XZ+DyptDL2G/29eDyLblgUrtz9nlixn8GnOni1uwrHvYL0N3IT+WXfcRdqvBEp7YXfoyXootb7cBfxr41NXKt/COdsRcX3oGOtpcIPrIbuax8IPbz6bCXfm7Xd2vcLGtUUHjXJoDFR3apOg8KOcITHtGyF8pbm1jcp+ymvzYyjuWsLoh3KLyjtkf9FS92wJ3gcze79kIccn7ZIacVrdD0ZdvXsZbXBfU7Oyi8JLKHA72T+/lCj/EO/7d3XFIYgkO+sT5Wsr3/1uTaGq3hRltdFuf9DugY8hq1y8mdM08fC13wO233/n0RnEsO+draWMkrWl0bW23wVrYo/ps3rYKt0RBULMLvqp3pbGd8bG8Lnaq+A3lYzSvfALx+38vmxq+AP280sIBdyfaVsEf7Bu1bDb/9AWzTImZ/QO+cntbrxbzScT4vk7YfEJEjom+7HVrLy+Ezyhp2LUpYtjCsYklbxDewYmW5Td9fWeYd6pnvd2oPyyi2QvzqdYx3DiUDXqP2MLPqZN72Y7BLn0hoBTwu+jar0eFAs5rdsqX+KrKyMlFq01+r8BqtK6vQg69b9MtKkpvCp0U6iR7rueWFHq5hgdd1b6RNRL7rmL4tmwmjxYEmNUJsXrIXbl6jtlJi4fuuj/LivCg+qi9+RNYspVEAPvA0SGHnRHb/rvBw98L5G1Va+N0QvWp/z3S8jCWvm1Rqrw5suKPFeYfKo/dX/GLUS7Qgs/h1qdpfqQn4BXDLL9SZSCL7x/RxeOf4DiXj1a5JQHd/ePrrZiI9LmxZoZrdtCJ1vVJe0w+Owk76rF+WKJYxAP0mzje46zViBzHQcbfSuRPGc+GtPaGQnekb6y+eq1oW5l6qS2pIZaC8lq9SarWwAnvwP13hDkDID33TbfK6vEk2sZ2npmB3/smstPCRnJPSIQdc1Ei6HHtYgh0ZHeoryT5QCWvvL3JkvfnoB4CVe5OEV1/f7rgOhNDlWUGZf8zZE40jO6r+/BmNbl9sZYegIlX6VfmxtJafnwc9JPXrD4D0z5IXtOo+8MYuZUfHEz3YgZfCd17aA3YqO+gkz7EXJFXO6GBZLZ/dbOMXTtbv9Qkqj102yG7woeyg8yy8CxPPwEeFT6wdW0b6/TbqKVv32+DvyG+1MXq9nKbHnuAM191VdIsN4ScJb7dATZ/E411bXh5A8Hb/xAEV3tglW5t+u40V0D1c2JKwxQpyo5ymHxzW7WUo4lf+UoK9Jsj+kuyJtYPHZWf6Vyd4Kvwn3/Tts+xcdqZn2JmddWRXoRKaXjbQE7r1Q59gj5HcFiPW/ukLlV3o7ivjufDFdt1eJ8O2WypdL6flU/osRf2Bn/gdhdD0ZLfHOrQ86J+PUHa1y1vjjlB4afokH7aj5etCV/MsInjY58tp+oFh2URNNxJL+cAnqDzvouXtyRl2rOlGZT/9Cjsn2I8K7K2sXej/jPcR48LDPjxQgh2bhWboul+e4FF6LrzYKdb+KbU7sZ+e0oDHQqbS9J3t8TZqnbePA/7OUPfp/TfO3Dsv6HnIi71l6W0s6sb245P/nkb20+OTI9g/RAPebBm5wnami9tuG4im7+9+y9+es3TE117wfLArtHPdTxEnwZ9PjtmOxbrbYrfbpMb2zttFIrcHSmh5KbvSVS92rftWR/sB1/1Y6GI/ZvvBPiZerR2J7ELXZAr/eH54sPuvcGh5W3ZOqLwf8GyX13ezdCmt2XrMeKcBHXZapj2ahTFbgasddGMPh/r5+aESXuHismf3xZXCq/1NsmVe3UFH3Q8Ojo4Rl8kx5eAAdd8vti8Zuyk8mr7rA35wWLdBD/SM3trtcJfVuQ8PD8Wu+MPDA+Sy9gdIZEfhH2PAlzDcxS50JLXPRrtgn2UHHjk+FLrYDxHQxb5r7Rtkr8f2/HbQwJcx4H+4IcNd6DH+EvZDioujZRe7eYGnrQOXOtrzTX9nqNTh/kCTs9eL7MCTEXRjPzzX/szaTeF909/4ocTh/uBc++ZZdh7xzgR0bfmO9hfevkD2WbUrXpv+9o+lDPdi+wLsT2HfeG3s0YCXYmv7M13Lfnm7Nj0G/EDXX90fasvLvv/WvoTrONhb1q6FD/I/fbxe6efYfym0o+kfP74z2PWT+TDchR7ZZ9WeO6W1eF/nPzX80zB0c0IPez3YZ9XOyR3ph7p9/fowtiNZO5/XLSzWI7vBk56cB7/H2Xeu8Em61ibbl+u4OQm74k3hu3+wG7wTHeosne0LBXbVgwntu9/fpcEH+Nuo5sbe5Au5yI50HPBdPtgNzRXaZ4N9cbG+TPaXajd8GMn7WzbvkF0XuY19HRczy/Xa4sIc2U3hs/bHwwNdPrN5aIe70BHQ5xYWa/VlXMKuNcVu4xKBv01DH22/S1yhfS2184CXyhv7I7IPlnioA17kQld7I7IbemC/ShP424n7d6F9FXuB1moL//KFF72nm4NdWYc6ici9HdMDtRpm51e17laebJFc4Q3+vQl/67etMyu/Sz3fCHZpei292JFgv16SXfAiF/rcQrA31pv379/vSMdk8tu3zVfrSIMeE2lw8NEr3k62Mx6fTOxo+urCnOBFj8T2bl7GDslwF7zIlf4I9mqt7nu+s93xg3DN5joeDWoQncP+tXU8ubYJfWc70lwje93bHwkeUTpFz2rLsUtmY/rcE8xOw746PT09M2PwKKnKyb1M/y17v+hbLVt62Gdmpqe57jTJ/kTwNmofKOOMFlG40qns2Hieeh504KHP0Vs0i96EHHDIKdVlH9KvrQGPxjd4yGem7+Fz8njH/vZP5vgVV/XWPjzQzZe4h7DbsBxfj8sOe71+9+7E5OTU1L17MzO5dlc5EEi9Cnu1ulyn53dSvS39zMy9e1NTk5OTE3fpHwnelL6c2xf9ZJevZOVCp56/OzExNjY2OT4+lcU73+6h258SHN1bDamzHsfIZ1J61it9anx8ZAyZmLhb48LPM96WHmX39vmhEm5cdJSDPs/0n0dHK5VbyMgI41XOz8uwfMXLlV5D6oyX0m/i37gsfWTkFqVSGf25+v/27mC3bSMIAzAp0pRKCraERkaLIIc8QVQDikWLCO1TEdTtra+QF+mL958hx0N7ZIZ0CLS73B/uobkoX2Z2lqRWsuKNnss+uZ1f5nU5PxquasDz1Sq5uFitLhnPctCl3SH/XeQ4KnFHRwaQGnjWf+2WvuEz/XJ1kcRJkq1YXyEnq+eyi72YbnvHiyjeyunVqnebYpnGESdu8P8wQOSPX5/kAB+fQudXWI/S//H4l+qRb0T/6SKOopgnT5ZDD3pHf9C3Yw/U8o09nsr+WVeXwjtyvNhuU2SLSHMB/EeSA0772p9cdJbfc8WPnyWoPuvb0svE/8b5iKpfdLacOMs37yDv6pkvfyWuRFUsJrXrv6+W3Mg1q0v6JboMx0KXdmc53450c3cE/r4tPTc+9H+z/sMvL09Hkx5w1RMfkWLw+ltPZU9hFzy5teRduf01+e/BBlzluA27b+XaRO1b2NL4MvPAp3z42fx673iRFZud6JkvITrbiwntjJfc9sg1K/qYAeAq56KDqfKuXkovevCR84fiod9Wqqc0bUj0Cj+TrfcEdsJzBI4ADnmeNa9j7dePDGe5tLuRWz3wpMfCR97zardJoNfat/6S/59/oqkCLSJwLflpuxa5TXx5DTfLpd1Fbu/ABP+F8aJHrrXlbe03UCqfUvEYRCazH2/Lo0ThO8jTnivh1XVzrN20u8A1pvR0L4z0fhYC+nyD4rNZwvR6SjvwCLEZznJs58l3Pl/CbpF/MfLuO7lm5vF1br8dSfP1lsGIyIleT2g/lpq25GbAGfsV3IAjsq+pHHDJ89LTpc6D6Ps+CqELn4qvqYk+pR2RgiOAJ0OOXZNb5C29A//EYb0tPfTMH3IiPl0WG5UzfcKeB1tW+madL9OBH66BoGb5i6ILXPVnS0/8YYfCF9myWHf5U9a9Xef5MkPFBya9onsuvoA9L7d6wd/xDQ5+BtqROM2WeQ44Z0o76aNxgR3tx3Khi9zmRen5KdBDDfy4T8DEcRzV1ZR24N/2yANT9yhFF7rKjV7x7SMw8Mk+PqD/p4lhP3boWvQB+OYZGHK1iFxMspbz9kr/1JfOAYpbwRdO2uPFWugUoffjtevF/n/5wvFxWRRqV/rwwsNe1XnkZGK2m7L34z2xRwXoYhf6WPsycjP5SDsi9kNr3zpsP77dfqqQbRa5mWUJ++FH7PXGVXu2JbvMedDH29dp5GbeZL9h+63Yk8jNpJtSh91Au455theRo0nWZD+MtGvL19XO1e09igsMO7XvR9qBd3aLwyb3fNCPWO6uj3k6ilqSXQo/tuUdHvM06GXBj7c7fAcrg/6H7O6OOjqkhAU/yn5DIbrro46GXTnKrmWnjnd51GHYbcku+LEt7/KoMwt+VMtjuTv6wEoWvGn6wS3v9qhD8h03/Q1nP6jl1e7szfuYBW9bvq5dX+604M0uN7Dl3X0+LYkLWfCU/biWd3u5y4Jv8fthLQ+687s7JTWX9ENb3t1nNpJk6C6nLV82dnef2Yxsetvybl/MD72PtS3vww43+NLOtvyD6zucbfrf5tTypukHtvyvHrR8Z9Jzhtldv4d7yvJZ08+p5QfexO99u7DRL7r8btP7di1//kZ239PyvMNVPlzLS1Lzvlx/y7v9YP7sFg+7xduW92jSUTIz7UzZ9aKu8mVzb7LQaUf4s2VH/Jt0PdPOPrHxbNL1nULQlhe7ywdJX0muR28Ub1u+hN2zspttrv/dV382uCa5TDuLR8v7ctCkZ5vTaWfLLi1f7Ty5lO+k2GnTAz+jslPh7bSTSSd2T8seLUzhzaRDy5/cPTk95ISp4JXuxwnaMYW3dm/LLoVX/F7p2vJell0Lr3iE6FL2k7dl58Iz/sx3gh2alvdxyGvhpes12vLlyduyNxd3Fq8tvyt8LTs/scWVrcXLV3B5dgNnCi940/Jl6d0NnHlq2eLnVnZ+j6Y8KV6/cA919+op3bnk2yc89PqFe/hTTy9rNOn6pHjOgeiwe7y/SZZbrOzTkfUHyIXu8/7WucARPOmFXno+6GSfOzEeegrTZ9HxbdcDD72EJsAMOp6SFBXk9F8rR7yf8Trr5cs2EdC9v6rpJgOe1FUFONHnsdhb/KmCXlLOiR7FWSV69Px8Gr5JVpGe+XOjRxHDKV7ft76SumJ+NM/MmE72KCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkZC75F7e7vhuRbdT0AAAAAElFTkSuQmCC">
        </div>
    </div>

    <div id="folderList">
        <main>
        </main>
    </div>

    <script>
        var animeList = document.querySelector("#animeList");
        var folderList = document.querySelector("#folderList");
        var folderListMain = folderList.querySelector("main");

        folderList.onclick = (e) => {
            if (!e.composedPath().includes(folderListMain)) {
                animeList.style.opacity = 1;
                animeList.style.pointerEvents = "unset";
                folderList.style.opacity = 0;
                folderList.style.pointerEvents = "none";
                folderListMain.style.opacity = 0;
                folderListMain.style.transform = "scale(0.8)";
            }
        }

        function show(folder) {
            animeList.style.opacity = 0.6;
            animeList.style.pointerEvents = "none";
            folderList.style.opacity = 1;
            folderList.style.pointerEvents = "unset";
            fetch(".?show=" + encodeURIComponent(folder)).then((response) => response.json())
            .then((data) => {
                folderListMain.innerHTML = "";
                var title = document.createElement("p");
                title.innerText = decodeURIComponent(folder);
                folderListMain.appendChild(title);
                var len = Object.keys(data).length/2;
                for (var x = 0; x < len; x++) {
                    var d = document.createElement("details");
                    var s = document.createElement("summary");
                    s.innerHTML = data[x];
                    d.appendChild(s);
                    data[data[x]].forEach(ep => {
                        var p = document.createElement("p");
                        if (ep.includes(".!!!WATCHED!!!.")) {
                            p.style.color = "rgb(249, 129, 149)";
                            ep = ep.split(".!!!WATCHED!!!.")[0];
                        }
                        var eep = ep;
                        [".mp4", ".webm", ".avi", ".av1", ".mkv", ".mov", ".m4v"].forEach(format => {
                            if (ep.includes(format)) {
                                eep = eep.split(format)[0];
                            }
                        });
                        p.innerHTML = eep;
                        p.onclick = (e) => {
                            showTime(folder + "/" + encodeURIComponent(e.target.parentElement.querySelector("summary").innerText) + "/" + encodeURIComponent(ep));
                            var what = e.target.parentElement.querySelector("summary").innerText + "uwu" + ep;
                            fetch(".?watched=" + encodeURIComponent(what) + "&folder=" + encodeURIComponent(folder) + "&add=" + "true");
                            p.style.color = "rgb(249,129,149)";
                        }
                        p.oncontextmenu = (e) => {
                            e.preventDefault();
                            if (p.style.color == "rgb(249, 129, 149)") {
                                p.style.color = "unset";
                            } else {
                                p.style.color = "rgb(249, 129, 149)";
                            }
                            var what = e.target.parentElement.querySelector("summary").innerText + "uwu" + ep;
                            fetch(".?watched=" + encodeURIComponent(what) + "&folder=" + encodeURIComponent(folder) + "&add=" + (p.style.color == "rgb(249, 129, 149)" ? "true" : "false"));
                        }
                        d.appendChild(p);
                    });
                    folderListMain.appendChild(d);
                }
                folderListMain.style.opacity = 1;
                folderListMain.style.transform = "scale(1)";
            });
        }

        function pin(e, folder) {
            e.style.order = getComputedStyle(e)["order"];
            e.classList.toggle("pinned");
            fetch(".?pinned=" + encodeURIComponent(folder) + "&add=" + (e.classList.contains("pinned") ? "true" : "false"));
        }

        function getOS() {
            //stolen from SO, lol
            var userAgent = window.navigator.userAgent,
                platform = window.navigator?.userAgentData?.platform || window.navigator.platform,
                macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
                windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
                iosPlatforms = ['iPhone', 'iPad', 'iPod'],
                os = null;

            if (macosPlatforms.indexOf(platform) !== -1) {
              return 'Mac OS';
            } else if (iosPlatforms.indexOf(platform) !== -1) {
              return 'iOS';
            } else if (windowsPlatforms.indexOf(platform) !== -1) {
              return 'Windows';
            } else if (/Android/.test(userAgent)) {
              return 'Android';
            } else {
              return 'Linux';
            }
        }           

        function showTime(file) {
            var good = 0;
            [".mp4", ".webm", ".avi", ".av1", ".mkv", ".mov", ".m4v"].forEach(format => {
                if (file.includes(format)) {
                    good = 1
                }
            });
            if (good == 1) {
                var a = window.location.href.split("://")
                file = a[0]+"://"+"USER:PASSWORD@"+a[1]+file; //edit this
                switch (getOS()) {
                    case "Mac OS":
                        window.location = "iina://weblink?url=" + file; //and this
                        break;
                    case "iOS":
                        window.location = "outplayer://" + file;
                        break;
                    case "Windows":
                        window.location = "mpv://play/" + btoa(file).replace(/\//g, "_").replace(/\+/g, "-");
                        break;
                    default:
                        window.location = "mpv://open?url=" + file
                        break;
                }
            } else {
                window.open(file, '_blank').focus();
            }
        }

        var search = {};

        var anime = document.querySelectorAll(".showCard");

        anime.forEach(e => {
            e.addEventListener("contextmenu", (e) => {e.preventDefault()});
            var t = e.querySelector("p").innerText.replace(/[^a-z0-9]+|\s+/gmi, "").toLowerCase();
            switch (t) {
                case "acchikocchi":
                    t += "|placetoplace";
                    break;
                case "cellsatwork":
                    t += "|hatarakusaibo";
                    break;
                case "anohanatheflowerwesawthatday":
                    t += "|anohimitahananonamaeobokutachiwamadashiranai";
                    break;
                case "fatekaleidlinerprismaillya":
                    t += "|prillya";
                    break;
                case "gabrieldropout":
                    t += "|gabdrop";
                    break;
                case "inoubattlewithineverydaylife":
                    t += "|whensupernaturalbattlesbecamecommonplace";
                    break;
                case "istheorderarabbit":
                    t += "|gochiusa|gochuumonwausagidesuka";
                    break;
                case "ishuzokureviewers":
                    t += "|interspeciesreviewers";
                    break;
                case "kaguyasamaloveiswar":
                    t += "|kaguyasamawakokurasetaitensaitachinorenaizunosen";
                    break;
                case "magiarecord":
                    t += "|puellamagimadokamagicasidestory";
                    break;
                case "ochikoborefruittart":
                    t += "|dropoutidolfruittart";
                    break;
                case "oniichanwaoshimai":
                    t += "|onimaiimnowyoursister|oniichanisdonefor"
                    break;
                case "oreimo":
                    t += "|mylittlesistercantbethiscute|orenoimotogakonnanikawaiiwakeganai";
                    break;
                case "princessconnectredive":
                    t += "|priconne";
                    break;
                case "sakuratrick":
                    t += "|gaygayhomosexualgay";
                    break;
                case "squidgirl":
                    t += "|shinryakuikamusume";
                    break;
                case "virtualsanwamiteiru":
                    t += "|virtualsanlooking";
                    break;
                case "watamote":
                    t += "|watashigamotenainowadokangaetemoomaeragawarui|nomatterhowilookatititsyouguysfaultimnotpopular";
                    break;
                case "watashinitenshigamaiorita":
                    t += "|wataten|anangelflewdowntome|thatonewiththeclingyloli";
                    break;
                case "yuruyuri":
                    t += "|yryr|yuruyurihappygolily";
                    break;
            }
            search[t] = e;
        });

        document.querySelector("input").addEventListener("keyup", (e) => {
            var searchstring = e.target.value.replace(/[^a-z0-9]+|\s+/gmi, "").toLowerCase();
            if (searchstring == "") {
                Object.values(search).forEach(e => {
                    e.style.display = "block";
                });
            } else {
                Object.keys(search).forEach(e => {
                    if (e.includes(searchstring)) {
                        search[e].style.display = "block";
                    } else {
                        search[e].style.display = "none";
                    }
                });
            }
        });
    </script>
</body>
</html>